import shelf from './shelf';
import * as storage from 'redux-storage'
import { combineReducers } from 'redux';
const rootReducer = storage.reducer(combineReducers({
    shelf
}))

export default rootReducer;