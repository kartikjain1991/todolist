export default(state = [], payload) => {
    switch (payload.type) {
        case 'add':
            return [...state, payload.item];
        case 'remove':
            let index = state.findIndex(item=>item.id === payload.item.id)
            if(index && index === -1){
                return state
            }
            return [...state.slice(0, index), ...state.slice(index + 1)]
            
        case 'check':  //not used..handled with more generic update case
            return state.map((item, index) => {
                if (item.id !== payload.item.id) {
                  // This isn't the item we care about - keep it as-is
                  return item
                }

                // Otherwise, this is the one we want - return an updated value
                return {
                  ...item,
                  "checked": !item.checked
                }
              })
        case 'update':
            return state.map((item, index) => {
                if(payload.items && payload.items[item.id]){
                    return {
                        ...item, 
                        ...payload.items[item.id]
                    }
                }

                return item

                // if (!payload.items[item.id]) {
                //   // This isn't the item we care about - keep it as-is
                //   return item
                // }
                
              })
        default:
            return state;
    }
};