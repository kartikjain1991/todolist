import React, { Component } from 'react';
import './App.css';

import Shelf from './components/shelf';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={"http://www.ivy.edu.au/wp-content/uploads/2015/05/Checklist.jpg"} className="App-logo" alt="logo" />
          <h2>To Do List</h2>
        </div>
        <div className="App-intro">
          <Shelf />
        </div>
      </div>
    );
  }
}

export default App;