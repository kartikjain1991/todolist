
import { createStore, applyMiddleware } from 'redux';
import * as storage from 'redux-storage'
import rootReducer from  './reducers';
import createEngine from 'redux-storage-engine-localstorage';
const engine = createEngine('my-save-key');
const middleware = storage.createMiddleware(engine);
const createStoreWithMiddleware = applyMiddleware(middleware)(createStore);
const store = createStoreWithMiddleware(rootReducer);
const load = storage.createLoader(engine);
load(store)
    .then((newState) => console.log('Loaded state:', newState))
    .catch(() => console.log('Failed to load previous state'));
//alert can be unprocessed and executed
// const defaultState = { 
//    shelf : [{"alert":"unprocessed","due":"2019-03-03T17:18","value":"shampoo","checked": false, "id": 1},
//             {"alert":"unprocessed","due":"","value":"chocolate","checked": true, "id": 2},
//             {"alert":"unprocessed","due":"2019-03-03T19:11","value":"yogurt","checked": false, "id": 3}
//           ]
// };
// export default(initialState=defaultState) => {
// 	console.log(initialState)
//     return createStore(rootReducer, initialState);
// }

export default store;