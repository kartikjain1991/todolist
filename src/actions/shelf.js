export const addToShelf = (item) => {
    console.log('adding item:', item);
    return {
        type: 'add',
        item
    };
}

export const removeFromShelf = (item) => {
    console.log('removing item:', item);
    return {
        type: 'remove',
        item
    };
}

export const checkUncheckItem = (item) => { //not used..handled with more generic update case
    console.log('updating item:', item);
    return {
        type: 'check',
        item
    };
}

export const updateItems = (items) => {
    console.log('updating items:', items);
    return {
        type: 'update',
        items
    };
}