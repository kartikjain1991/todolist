import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import * as shelfActions from '../actions/shelf';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import compose from 'recompose/compose';
import SearchIcon from '@material-ui/icons/Search';
import NoteAdd from '@material-ui/icons/NoteAdd'
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle'
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, TimePicker, DatePicker } from 'material-ui-pickers';
import MySnackbarContentWrapper from './snackbar'
import Snackbar from '@material-ui/core/Snackbar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import DeleteForever from '@material-ui/icons/DeleteForever';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {styles} from './shelfStyles';
import Typography from '@material-ui/core/Typography';




class Shelf extends Component {
  constructor(props) {
    super(props)
    this.onAddItemToCart = this.onAddItemToCart.bind(this);
    this.onRemoveItemFromCart = this.onRemoveItemFromCart.bind(this);
    this.onCheckUncheckItemInCart = this.onCheckUncheckItemInCart.bind(this)
    this.processUpcomingDueDates = this.processUpcomingDueDates.bind(this)
    this.timeoutFunction = this.timeoutFunction.bind(this)
    this.state = {
      newitem: '',
      newitemDue: null,
      openDialogueBox: false,
      openSnackBar: false,
      snackBarMessage: '',
      snackBarVariant: 'success',
      anchorEl: null,
      sort: '',
      search: ''
    }
    document.addEventListener("keydown", this.handleKeyDown.bind(this))
    this.processUpcomingDueDates()

  }

  handleKeyDown(e) {
    
   if (e.which === 78) {
      console.log("N pressed for new item")
      this.handleDialogueBox(true)
   } 
   // if (e.key === "a" && e.key === 1) {
   //    console.log("A pressed for removing item")
   //    console.log(e)
   // }
  }

  handleSearchInput = event =>{
    if(event){
      this.setState({ search: event.target.value });
    }
    else
      this.setState({ search: "" });
  };

  handleMenuClick = event => {
    if(event)
      this.setState({ anchorEl: event.currentTarget });
    else
      this.setState({ anchorEl: null });
  };

  updateSnackBarState = (message, variant)=>{
    this.setState({snackBarMessage: message, snackBarVariant: variant, openSnackBar: true})
  }

  handleSnackBarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ openSnackBar: false });
  };

  handleDialogueBox = evt => {
    this.setState({ "openDialogueBox": evt });
  }

  timeoutFunction = (item, msg) => {
    console.log("Executing timeout for item", item.value)
    this.updateSnackBarState(msg+item.value, "info")
    this.props.actions.updateItems({[item.id]: {"alert": "executed", "timeout": null}})
  }

  processUpcomingDueDates = () => {
    console.log("inside processUpcomingDueDates")
    let itemIdsToBeUpdated = {}
    this.props.shelf.map((item, index)=>{
      if(item.due && item.alert !== 'executed'){
        itemIdsToBeUpdated[item.id] = this.setAlertForSingleItem(item)
      }
      return item
    })
    this.props.actions.updateItems(itemIdsToBeUpdated)
    setInterval(this.processUpcomingDueDates, 15*60*1000)
  }

  //generic function for handling single/multiple items at once
  setAlertForSingleItem = item => {
    if(!item.due)
      return {}
    let dueDate = new Date(item.due)
    let currentDateMinus5Min = new Date()
    currentDateMinus5Min.setMinutes(currentDateMinus5Min.getMinutes()-5)
    let timeDiff = dueDate - currentDateMinus5Min //in miliseconds
    let timeForAlert = dueDate - new Date()
    if(timeDiff < 0){
      setTimeout(()=>{
        this.timeoutFunction(item, "Missed your event? ")
      }, timeForAlert)
      if(item.timeout)
        clearTimeout(item.timeout) //ensuring if any old interval set then its cleared
      return {"alert": "executed", timeout: null}
       //execute unexecuted tasks that user missed          
    }else if(timeDiff > 0 && timeDiff <= 900000 ){ //15 minutes
      let timeout = setTimeout(()=>{
        this.timeoutFunction(item, "Time's up! ")
      }, timeForAlert)
      return {"timeout": timeout}
    }
    return {}
  }

  onAddItemToCart = () => {      
      if(this.state.newitem){   
          let newid = new Date().getTime()
          
          if(this.state.newitemDue){
            let currentDate = new Date()
            currentDate.setHours(currentDate.getHours()-1)   //this ensures if page is open for upto 1 hour then new item can be added without error that due date cant be before today
            let newitemDue = new Date(this.state.newitemDue)
            if((newitemDue - currentDate)<0){
              this.setState({"newitemDue":null})
              this.updateSnackBarState("Due Date cant be before current date time!", "error")
            }
          }
          
          if(!this.props.shelf.find(item=>item.id===newid) ){
            let newitem = {
              "value": this.state.newitem,
              "checked": false,
              "id": this.generateUniqueId(), //unique id
              "due": this.state.newitemDue,
              "alert": "unprocessed"
            }
            newitem = {...newitem, ...this.setAlertForSingleItem(newitem)}
            this.props.actions.addToShelf(newitem)
            this.setState({"newitem": '', 'newitemDue': null})
          }
          this.handleDialogueBox(false)
      }else{
        this.updateSnackBarState("No Task?", "error")
      }

  }

  generateUniqueId = () => {
    let newid = new Date().getTime()
    if(!this.props.shelf.find(item=>item.id===newid)){
      return newid
    }else{
      return this.generateUniqueId()
    }
  }

  onRemoveItemFromCart = item => {
    let timeout = item.timeout
    if(timeout)
      clearTimeout(timeout)
    this.props.actions.removeFromShelf(item)
  }

  updateInputValue = (evt, field) => {
     this.setState({
      [field]: evt.target.value
    });
  }

  handleDateChange = date => {
    this.setState({ newitemDue: date });
  };

  getDateStatement = dateobj => {
    if(!dateobj){
      return ""
    }
    let msg = ""
    let currentDate = new Date()
    let dueDate = new Date(dateobj)
    if(dueDate-currentDate>0){
      msg += "Due in "
    }else{
      msg += "Past Due Date by "
    }
    let timeDiff = Math.abs(dueDate.getTime() - currentDate.getTime());
    let diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
    if(diffDays>1)
      msg += diffDays + " Days"
    if(diffDays === 1)
      msg += diffDays + " Day"
    if(diffDays < 1){

      let hours = Math.ceil(timeDiff / (1000 * 3600))
      hours = hours>1?(hours+" hours"):" 1 Hour"
      msg = msg+"Less than "+ hours
    }
    return msg
  }

  onCheckUncheckItemInCart = item =>{
    let dataToBeUpdated = {[item.id]: {}}
    if(!item.checked){
      //remove the timer
      if(item.timeout)
        clearTimeout(item.timeout)
      dataToBeUpdated[item.id] = {"timeout": null, "alert": "executed"}
    }else{
      //add the timer
      dataToBeUpdated[item.id] = {...dataToBeUpdated[item.id], ...this.setAlertForSingleItem(item), "alert": "unprocessed"}
    }
    dataToBeUpdated[item.id]["checked"] = !item.checked
    this.props.actions.updateItems(dataToBeUpdated)
  }

  sortShelf = (sortType) => {
    let shelfItems = [].concat(this.props.shelf)
    shelfItems.sort(function (itema, itemb) {
      let currentDate = new Date()
      if(itema.checked){
        if(itemb.checked){
          //both items are checked
          let dueDateA = itema.due
          let dueDateB = itemb.due
          if(dueDateA && dueDateB){
            //both dates are present ..the one closer to current date will be more urgent
            dueDateA = new Date(dueDateA)
            dueDateB = new Date(dueDateB)
            let timeDiffA = dueDateA.getTime() - currentDate.getTime();
            let timeDiffB = dueDateB.getTime() - currentDate.getTime();
            if(sortType === "urgency")
              return ((Math.abs(timeDiffA)-Math.abs(timeDiffB))<0?-1:1)
            else
              return ((timeDiffA-timeDiffB)<0?-1:1)
          }else if(dueDateA && !dueDateB){ //the one with date is more urgent
            return -1 
          }else if(!dueDateA && dueDateB)
            return 1 
        }else{
          //itemb is unchecked
          return 1
        }
      }else{
        if(itemb.checked){
          //item a is unchecked
          return -1
        }else{
          //both items are unchecked
          //both items are checked
          let dueDateA = itema.due
          let dueDateB = itemb.due
          if(dueDateA && dueDateB){
             //both dates are present ..the one closer to current date will be more urgent
             dueDateA = new Date(dueDateA)
             dueDateB = new Date(dueDateB)
             let timeDiffA = dueDateA.getTime() - currentDate.getTime();
             let timeDiffB = dueDateB.getTime() - currentDate.getTime();
             if(sortType === "urgency")
              return ((Math.abs(timeDiffA)-Math.abs(timeDiffB))<0?-1:1)
             else
              return ((timeDiffA-timeDiffB)<0?-1:1)
          }else if(dueDateA && !dueDateB){ //the one with date is more urgent
            return -1 
          }else if(!dueDateA && dueDateB)
            return 1
        }
      }     
      return 1
    });
    return shelfItems
  }

  filterShelf = () => {
    console.log("inside filter")
    let shelfItems = [].concat(this.props.shelf)
    shelfItems = shelfItems.filter(item => {
      return this.fuzzy_match(item.value,this.state.search)
    })
    return shelfItems
  } 

  fuzzy_match = (str,pattern) => {
      pattern = pattern.split("").reduce(function(a,b){ return a+".*"+b; });
      return (new RegExp(pattern)).test(str);
  }

  updateSortState = (sort) => {
    this.setState({sort: sort, anchorEl: null})
  }

  getSortedItems = shelfItems => {
    console.log("inside sorting")
    let updatedItems = shelfItems
    if(this.state.search){
      updatedItems = this.filterShelf()
    }
    if(this.state.sort){
      updatedItems = this.sortShelf(this.state.sort)
    }
    return updatedItems
  }

  render(){
    const { classes, shelf } = this.props;
    const {anchorEl, openSnackBar, snackBarVariant, openDialogueBox, newitem, newitemDue, snackBarMessage, search} = this.state;
    return (
      <div className={classes.root}>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={() => this.handleMenuClick(null)}
        >
          <MenuItem onClick={() => this.updateSortState("urgency")}>Show Urgent Ones First</MenuItem>
          <MenuItem onClick={() => this.updateSortState("duedate")}>Show Oldest Ones First</MenuItem>
          <MenuItem onClick={() => this.updateSortState("")}>Clear</MenuItem>
        </Menu>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={openSnackBar}
          autoHideDuration={6000}
          onClose={this.handleSnackBarClose}
        >
          <MySnackbarContentWrapper
            onClose={this.handleSnackBarClose}
            variant={snackBarVariant}
            message={snackBarMessage}
          />
        </Snackbar>
        <Dialog
          open={openDialogueBox}
          onClose={() => this.handleDialogueBox(false)}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add New Note</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please Enter the text. Optionally set a reminder for yourself.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="newtask"
              label="Task"
              type="text"
              fullWidth
              required
              value={newitem}
              onChange={evt => this.updateInputValue(evt, "newitem")}
            />
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container className={classes.grid} justify="space-around">
          <DatePicker
            margin="normal"
            label="Choose a date"
            value={newitemDue}
            onChange={this.handleDateChange}
            minDate={new Date()}
            clearable={true}
            // cancelLabel={"Cancel"}
          />
          <TimePicker
            margin="normal"
            label="Choose a time"
            value={newitemDue}
            onChange={this.handleDateChange}
            clearable={true}
          />
        </Grid>
      </MuiPickersUtilsProvider>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.handleDialogueBox(false)} color="primary">
              Cancel
            </Button>
            <Button onClick={() => this.onAddItemToCart()} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
        <Paper className={classes.paper}>
          <Grid container spacing={16}>
            <Grid item xs={12} sm container>
              <Grid item xs container direction="column" spacing={16}>
                <Grid item xs>
                  <div className={classes.search}>
                    <div className={classes.searchIcon}>
                      <SearchIcon />
                    </div>
                    <Input
                      placeholder="Search…"
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                      }}
                      onChange={this.handleSearchInput}
                      value={search}
                    />
                  </div>
                  
                </Grid>
                
              </Grid>
              <Grid item>
              <Button
                aria-owns={anchorEl ? 'simple-menu' : undefined}
                aria-haspopup="true"
                onClick={this.handleMenuClick}
              >
                SORT
              </Button>
              </Grid>
              <Grid item>
              <div >
                <NoteAdd onClick={() => this.handleDialogueBox(true)}/>
              </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid container spacing={16} direction="column">
            <Grid item xs container direction="row" spacing={16}>
                <List className={classes.root}>
                    {shelf.length>0?this.getSortedItems(shelf).map(item => (
                      <ListItem key={item.value} dense button >
                        <Checkbox
                          checked={item.checked}
                          tabIndex={-1}
                          disableRipple
                          onChange={() => this.onCheckUncheckItemInCart(item)}
                        />
                        <ListItemText primary={item.value} secondary={!item.checked?this.getDateStatement(item.due):""}/>
                        <ListItemSecondaryAction>
                          <IconButton aria-label="Delete" onClick={() => this.onRemoveItemFromCart(item)}>
                            <DeleteForever  />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                    )): 
                      <Typography variant="h5" component="h3">
                                            <br />
                                              Start Adding Items!!
                                            </Typography>
                  }
              </List>
            </Grid>
            <Grid item xs container direction="column" spacing={16}></Grid>
          </Grid>
        </Paper>

      </div>
       
      );
  }




}

function mapStateToProps(state, props) {
    return {
        shelf: state.shelf
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(shelfActions, dispatch)
    }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Shelf);

// export default connect(mapStateToProps, mapDispatchToProps)(Shelf);

// export default Shelf;
